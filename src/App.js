import React, { useEffect, useState } from "react";
import { createWorker } from "tesseract.js";
import "./App.css";
import Camera from "./Camera";
import Container from "react-bootstrap/Container";
import Majezik from "./Majezik.js";
import Vermidon from "./Vermidon.js";
import Furacin from "./Furacin";
import Naprosyn from "./Naprosyn";
import Novalgin from "./Novalgin";
import Parol from "./Parol";
import Strepsils from "./Strepsils";
import Talcid from "./Talcid";
import Tylolhot from "./Tylolhot";
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";
import Loader from "react-loader-spinner";
import Sky from "react-sky";
import myImage from "./images/vermidon+.png";

function App() {
  const worker = createWorker({
    logger: m => console.log(m)
  });
  const doOCR = async () => {
    await worker.load();
    await worker.loadLanguage("eng");
    await worker.initialize("eng");
    const {
      data: { text }
    } = await worker.recognize(photo);
    setOcr(text);
  };
  const [ocr, setOcr] = useState("Waiting");
  useEffect(() => {
    doOCR();
  });

  const [photo, setPhoto] = useState();

  const photoHandler = photo => {
    let photoUrl = URL.createObjectURL(photo[0]) || null;

    setPhoto(photoUrl);
    console.log(photo);
  };

  console.log(photo);
  return (
    <div className="containerAna">
      <Sky
        images={{
          /* FORMAT AS FOLLOWS */
          0: "https://image.flaticon.com/icons/png/512/991/991884.png" /* You can pass as many images as you want */,
          1: "https://cdn4.iconfinder.com/data/icons/health-and-fitness/100/medicine-08-512.png",
          2: "https://image.flaticon.com/icons/png/512/773/premium/773795.png"
        }}
        how={
          130
        } /* Pass the number of images Sky will render chosing randomly */
        time={40} /* time of animation */
        size={"100px"} /* size of the rendered images */
        background={"palettedvioletred"} /* color of background */
      />

      <Container fluid style={{ padding: "1%" }}>
        <div>
          {" "}
          <p
            style={{
              marginLeft: "42%",
              color: "white",
              fontWeight: "bolder",
              fontSize: "180%"
            }}
          >
            Prospektusum.com
          </p>
          <image />
        </div>

        <Camera handlePhotoChange={photoHandler} />
        <div style={{ marginLeft: "45%", marginTop: "2%" }}>
          {" "}
          {photo && ocr === "Waiting" ? (
            <div>
              <Loader type="Puff" color="#fc03ba" height={100} width={100} />
              <h2>Aranıyor...</h2>
            </div>
          ) : null}
        </div>
        {/* <p>{ocr}</p> */}
        {ocr.includes("Vermidon") ? (
          <div>
            <Vermidon />
          </div>
        ) : null}
        {ocr.includes("ajezik") ? (
          <div>
            <Majezik />
          </div>
        ) : null}
        {ocr.includes("Furacin") ? (
          <div>
            <Furacin />
          </div>
        ) : null}
        {ocr.includes("Naprosyn") ? (
          <div>
            <Naprosyn />
          </div>
        ) : null}
        {ocr.includes("Novalgin") ? (
          <div>
            <Novalgin />
          </div>
        ) : null}
        {ocr.includes("Parol") ? (
          <div>
            <Parol />
          </div>
        ) : null}
        {ocr.includes("repsils") ? (
          <div>
            <Strepsils />
          </div>
        ) : null}
        {ocr.includes("Tal") || ocr.includes("cid") ? (
          <div>
            <Talcid />
          </div>
        ) : null}
        {ocr.includes("TYLOL") ? (
          <div>
            <Tylolhot />
          </div>
        ) : null}
      </Container>
    </div>
  );
}

export default App;
